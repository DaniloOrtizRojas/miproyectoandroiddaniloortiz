package com.example.myapplication.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Entidades.Eventos;

import com.example.myapplication.R;

import java.util.ArrayList;

public class AdapterEventos extends RecyclerView.Adapter<AdapterEventos.ViewHolder> implements View.OnClickListener {

    LayoutInflater inflater;
    ArrayList<Eventos> model;

    private View.OnClickListener listener;

    public AdapterEventos(Context context, ArrayList<Eventos> model){
        this.inflater = LayoutInflater.from(context);
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.lista_eventos, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    public void setOnclickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String nombre_evento = model.get(position).getNombre_evento();
        String organizador = model.get(position).getOrganizador();
        String ubicacion = model.get(position).getUbicacion();

        int img_evento = model.get(position).getImg_del_lugar();

        holder.nombre_evento.setText(nombre_evento);
        holder.organizador.setText(organizador);
        holder.ubicacion.setText(ubicacion);
        holder.imagen_evento.setImageResource(img_evento);
    }


    @Override
    public int getItemCount() {

        return model.size();
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            listener.onClick(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nombre_evento, organizador,ubicacion;
        ImageView imagen_evento;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            nombre_evento = itemView.findViewById(R.id.nombres_evento);
            organizador= itemView.findViewById(R.id.organizador);
            ubicacion= itemView.findViewById(R.id.ubicacion);

            imagen_evento = itemView.findViewById(R.id.imagen_lugar_evento);
        }

    }

}
