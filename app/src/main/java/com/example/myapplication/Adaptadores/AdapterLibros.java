package com.example.myapplication.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Entidades.Eventos;
import com.example.myapplication.Entidades.Libros;
import com.example.myapplication.R;

import java.util.ArrayList;

public class AdapterLibros extends RecyclerView.Adapter<AdapterLibros.ViewHolder> implements View.OnClickListener {

    LayoutInflater inflater;
    ArrayList<Libros> model;

    private View.OnClickListener listener;

    public AdapterLibros(Context context, ArrayList<Libros> model){
        this.inflater = LayoutInflater.from(context);
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.lista_libros, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    public void setOnclickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String nombres = model.get(position).getNombre();
        String autores = model.get(position).getAutores();
        int imageid = model.get(position).getImagenid();
        holder.nombres.setText(nombres);
        holder.autores.setText(autores);
        holder.imagen.setImageResource(imageid);
    }


    @Override
    public int getItemCount() {
        return model.size();
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            listener.onClick(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nombres, autores;
        ImageView imagen;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            nombres = itemView.findViewById(R.id.nombres);
            autores= itemView.findViewById(R.id.Autores);
            imagen = itemView.findViewById(R.id.imagen_libro);
        }

    }

}
