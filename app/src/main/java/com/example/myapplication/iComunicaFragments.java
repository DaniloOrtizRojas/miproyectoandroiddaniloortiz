package com.example.myapplication;

import com.example.myapplication.Entidades.Eventos;
import com.example.myapplication.Entidades.Libros;

public interface iComunicaFragments {
    public  void enviarLibros(Libros libros) ;
    public  void enviarEvento(Eventos eventos);

}
