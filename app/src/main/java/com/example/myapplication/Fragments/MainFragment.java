package com.example.myapplication.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adaptadores.AdapterEventos;

import com.example.myapplication.Entidades.Eventos;
import com.example.myapplication.R;
import com.example.myapplication.iComunicaFragments;

import java.util.ArrayList;

public class MainFragment extends Fragment {
    AdapterEventos adapterEventos;
    RecyclerView recyclerViewEventos;
    ArrayList<Eventos> listaEventos;

    Activity actividad;
    iComunicaFragments interfaceComunicaFragments;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment,container,false);

        recyclerViewEventos = view.findViewById(R.id.recyclerView2);
        listaEventos = new ArrayList<>();
        //cargar la lista
        cargarLista();
        mostrarData();
        return view;
    }

    private void cargarLista() {

        listaEventos.add(new Eventos("Feria Del Libro","Colegio Mariano de Schoenstatt","El colegio","13/05/2022","14/05/2022",R.drawable.feria1));
        listaEventos.add(new Eventos("Feria Del Librp 1","Rambla de Nogalte Puerto Lumbreras","Pepito","04/11/2022","06/11/2022",R.drawable.feria2));
        listaEventos.add(new Eventos("Feria Internacional del Libro","Macul,Calle1","el alcalde","11/10/2019","20/10/2019",R.drawable.feria3));
        listaEventos.add(new Eventos("Feria del libro en la GAM","Centro Cultural Gabriela Mistral","Muchas personas","23/04/2022 11:00am","23/04/2022 17:45",R.drawable.feria4));
        listaEventos.add(new Eventos("Feria del libro VILLA ALEMANA","Centro Cultural Gabriela Mistral","Paquita y su grupo","05/08/2022 ","07/08/2022",R.drawable.feria5));
        //listaEventos.add(new Eventos("Feria del libro Valdivia","Av Pedro Montt esquina Errázuriz,Valdivia, Región de los Ríos","Corporacion Cultural Municipal Vañdivia","9/12/2022 desde las 18:00 horas","14/12/2022",R.drawable.feria6));
        //listaEventos.add(new Eventos("Feria del libro 2","La Serena, Avda. Fco de Aguirre 117, 141, La Serena, Coquimbo","colegio javiera carrera","04/06/2019","16/06/2019",R.drawable.feria7));


    }

    private void mostrarData(){
        recyclerViewEventos.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterEventos = new AdapterEventos(getContext(), listaEventos);
        recyclerViewEventos.setAdapter(adapterEventos);

        adapterEventos.setOnclickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre_evento = listaEventos.get(recyclerViewEventos.getChildAdapterPosition(view)).getNombre_evento();
                Toast.makeText(getContext(), "Selecciono:"+nombre_evento, Toast.LENGTH_SHORT).show();
                interfaceComunicaFragments.enviarEvento(listaEventos.get(recyclerViewEventos.getChildAdapterPosition(view)));
            }
        });


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.actividad = (Activity) context;
            interfaceComunicaFragments = (iComunicaFragments) this .actividad;


        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}