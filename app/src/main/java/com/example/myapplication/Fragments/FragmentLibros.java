package com.example.myapplication.Fragments;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adaptadores.AdapterLibros;
import com.example.myapplication.Entidades.Libros;
import com.example.myapplication.R;
import com.example.myapplication.iComunicaFragments;

import java.util.ArrayList;

public class FragmentLibros extends Fragment {
 AdapterLibros adapterLibros;
 RecyclerView recyclerViewLibros;
 ArrayList<Libros> listaLibros;

 Activity actividad;
 iComunicaFragments interfaceComunicaFragments;

 @Nullable
 @Override
 public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
  View view = inflater.inflate(R.layout.libros_fragment,container,false);
  recyclerViewLibros = view.findViewById(R.id.recyclerView);
  listaLibros = new ArrayList<>();
  //cargar la lista
  cargarLista();
  mostrarData();

  return view;
 }

 private void cargarLista() {
  listaLibros.add(new Libros("Don Quijote de la mancha","Miguel de Servantes","El ingenioso hidalgo don Quijote de la Mancha narra las aventuras de Alonso Quijano, un hidalgo pobre que de tanto leer novelas de caballería acaba enloqueciendo y creyendo ser un caballero andante, nombrándose a sí mismo como don Quijote de la Mancha.","Novela psicológica",R.drawable.donquijotedelamancha));
  listaLibros.add(new Libros("El Alquimista","Pablo Coelho","relata las aventuras de Santiago, un joven pastor andaluz que viaja desde su tierra natal hacia el desierto egipcio en busca de un tesoro oculto en las pirámides.","Novela narrativa, ficción",R.drawable.elalquimista));
  listaLibros.add(new Libros("El Principito","Antoine de Sanint-Exupery","trata de la historia de un pequeño príncipe que parte de su asteroide a una travesía por el universo, en la cual descubre la extraña forma en que los adultos ven la vida y comprende el valor del amor y la amistad.","Novela filosófica y novela infantil",R.drawable.elprincipito));
  listaLibros.add(new Libros("El caballero de la armadura oxidada","Robert Fisher","El protagonista, un caballero deslumbrado por el brillo de su armadura, a pesar de ser bueno, generoso y amoroso, no consigue comprender y valorar con profundidad las cosas que suceden a su alrededor. Su armadura se va oxidando hasta que deja de brillar y no puede quitársela.","Ficcion",R.drawable.caballerodelaarmaduraoxidada));
  listaLibros.add(new Libros("El maravilloso mago de Oz","L. Frank Baum","La historia de El Mago de Oz narra las aventuras de Dorothy Gale, una huérfana que vive en una granja con sus tíos y con Totó, su perrito. Un día, una vecina es mordida por Totó quien, al ver las posibles consecuencias de lo que ha hecho, decide escaparse de casa.","Novela infantil, fantastica",R.drawable.oz));
  //listaLibros.add(new Libros("Romero y Julieta","William Shakespeare","En Verona, dos jóvenes enamorados, de dos familias enemigas, son víctimas de una situación de odio y violencia que ni desean ni pueden remediar. En una de esas tardes de verano en que el calor «inflama la sangre», Romeo, recién casado en secreto con su amada Julieta, mata al primo de ésta.","Tragedia",R.drawable.romeoyjulieta));
  //listaLibros.add(new Libros("El Pequeño Vampiro","Angela Sommer-Bodenburg","Anton es un niño que vive junto a sus padres en un pueblo de Alemania. Lleva una vida convencional, sin incidentes ni cosas raras, pero una noche, mientras se encuentra leyendo, un ser extraño y terrorífico aparece en el alféizar de la ventana de su habitación, que está en un sexto piso. ¡Es un vampiro!","Ficcion",R.drawable.elpequenovanpiro));

}
 private void mostrarData(){
  recyclerViewLibros.setLayoutManager(new LinearLayoutManager(getContext()));
  adapterLibros = new AdapterLibros(getContext(), listaLibros);
  recyclerViewLibros.setAdapter(adapterLibros);

  adapterLibros.setOnclickListener(new View.OnClickListener() {
   @Override
   public void onClick(View view) {
    String nombre = listaLibros.get(recyclerViewLibros.getChildAdapterPosition(view)).getNombre();
    Toast.makeText(getContext(), "Selecciono:"+nombre, Toast.LENGTH_SHORT).show();
    interfaceComunicaFragments.enviarLibros(listaLibros.get(recyclerViewLibros.getChildAdapterPosition(view)));
   }
  });


 }

 @Override
 public void onAttach(@NonNull Context context) {
  super.onAttach(context);
  if (context instanceof Activity){
   this.actividad = (Activity) context;
     interfaceComunicaFragments = (iComunicaFragments) this .actividad;


  }


 }

 @Override
 public void onDetach() {
  super.onDetach();
 }
}