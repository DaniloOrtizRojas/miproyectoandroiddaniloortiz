package com.example.myapplication.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.Entidades.Libros;
import com.example.myapplication.R;

public class DetalleLibrosFragment extends Fragment {
    TextView nombreDetalle,autorDetalle,resumenDetalle,categoriaDetalle;
    ImageView imagenDetalle;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detalle_libros_fragment,container,false);
        nombreDetalle = view.findViewById(R.id.nombre_detalle);
        autorDetalle = view.findViewById(R.id.autor_detalle);
        resumenDetalle = view.findViewById(R.id.resumen_detalle);
        categoriaDetalle = view.findViewById(R.id.categoria_detalle);
        imagenDetalle = view.findViewById(R.id.imagen_detalle);
        //Crear bundle para recibir el objeto enviado por parametro.
        Bundle objetoPersona = getArguments();
        Libros libros = null;;
        //validacion para verificar si existen argumentos para mostrar
        if(objetoPersona !=null){
            libros = (Libros) objetoPersona.getSerializable("objeto");
            imagenDetalle.setImageResource(libros.getImagenid());
            nombreDetalle.setText(libros.getNombre());
            autorDetalle.setText(libros.getAutores());
            resumenDetalle.setText(libros.getResumen());
            categoriaDetalle.setText(libros.getCategoria());
        }
        return view;
    }
}
