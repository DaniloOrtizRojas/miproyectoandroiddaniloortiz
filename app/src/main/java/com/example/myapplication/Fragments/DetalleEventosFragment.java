package com.example.myapplication.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.Entidades.Eventos;

import com.example.myapplication.R;

public class DetalleEventosFragment extends Fragment {

    TextView nombreEventoDetalle,ubicacionDetalle,organizadorDetalle,fechainicioDetalle,fechaterminoDetalle;
    ImageView imagenEventoDetalle;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detalle_eventos_fragment,container,false);
        nombreEventoDetalle = view.findViewById(R.id.nombre_evento_detalle);
        ubicacionDetalle = view.findViewById(R.id.ubicacion_detalle);
        organizadorDetalle = view.findViewById(R.id.organizador_detalle);
        fechainicioDetalle = view.findViewById(R.id.fecha_inicio_detalle);
        fechaterminoDetalle = view.findViewById(R.id.fecha_termino_detalle);

        imagenEventoDetalle = view.findViewById(R.id.imagen_evento_detalle);
        //Crear bundle para recibir el objeto enviado por parametro.
        Bundle objetoEvento = getArguments();
        Eventos eventos = null;;
        //validacion para verificar si existen argumentos para mostrar
        if(objetoEvento !=null){
            eventos= (Eventos) objetoEvento.getSerializable("objeto");
            imagenEventoDetalle.setImageResource(eventos.getImg_del_lugar());
            nombreEventoDetalle.setText(eventos.getNombre_evento());
            ubicacionDetalle.setText(eventos.getUbicacion());
            organizadorDetalle.setText(eventos.getOrganizador());
            fechainicioDetalle.setText(eventos.getFecha_inicio());
            fechaterminoDetalle.setText(eventos.getFecha_termino());
        }
        return view;
    }
}

