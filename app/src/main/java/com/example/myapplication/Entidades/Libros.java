package com.example.myapplication.Entidades;

import java.io.Serializable;

public class Libros implements Serializable {
    private String nombre;
    private String autores;
    private String resumen;
    private String categoria;

    private int imagenid;

    public Libros(){

    }

    public Libros(String nombre, String autores,String resumen, String categoria,int imagenid) {
        this.nombre = nombre;
        this. autores= autores;
        this. resumen= resumen;
        this.categoria = categoria;
        this.imagenid = imagenid;
    }

    public String getNombre() {

        return nombre;
    }

    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    public String getAutores() {

        return autores;
    }

    public void setAutores(String autores) {

        this.autores = autores;
    }

    public  String getResumen(){
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public  String getCategoria(){
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria= categoria;
    }

    public int getImagenid() {
        return imagenid;
    }

    public void setImagenid(int imagenid) {

        this.imagenid = imagenid;
    }
}

