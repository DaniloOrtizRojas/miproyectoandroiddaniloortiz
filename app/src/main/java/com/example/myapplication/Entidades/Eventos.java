package com.example.myapplication.Entidades;

import java.io.Serializable;

public class Eventos implements Serializable {

   private String nombre_evento;
    private String ubicacion;
    private String organizador;
    private String fecha_inicio;
    private String fecha_termino;
    private  int img_del_lugar;


    public Eventos() {
    }

    public Eventos(String nombre_evento, String ubicacion, String organizador, String fecha_inicio, String fecha_termino, int img_del_lugar) {
        this.nombre_evento = nombre_evento;
        this.ubicacion = ubicacion;
        this.organizador = organizador;
        this.fecha_inicio = fecha_inicio;
        this.fecha_termino = fecha_termino;
        this.img_del_lugar = img_del_lugar;
    }

    public String getNombre_evento() {
        return nombre_evento;
    }

    public void setNombre_evento(String nombre_evento) {
        this.nombre_evento = nombre_evento;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getOrganizador() {
        return organizador;
    }

    public void setOrganizador(String organizador) {
        this.organizador = organizador;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_termino() {
        return fecha_termino;
    }

    public void setFecha_termino(String fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public int getImg_del_lugar() {
        return img_del_lugar;
    }

    public void setImg_del_lugar(int img_del_lugar) {
        this.img_del_lugar = img_del_lugar;
    }
}
